public class DeltaResult {
		/*
		 * This class represents the return value of the lookup function of the turing machine
		 * It also exports some simple-functionality
		 */
	
		public int state;
		public TapeSymbol symbol;
		public Direction direction;

		
		public DeltaResult(int state, TapeSymbol symbol, Direction direction) {
			this.state = state;
			this.symbol = symbol;
			this.direction = direction;
		}

		// delta table for '+' function: arg1 + arg2
		// input (args in unary, head on first field of arg1): arg1 BLANK arg2
		// output: number starting with head
		public static final DeltaResult[][] deltaTablePlus = { { new DeltaResult(0, TapeSymbol.BLANK, Direction.R), new DeltaResult(1, TapeSymbol.BLANK, Direction.R) }, // remove first one
			{ new DeltaResult(2, TapeSymbol.ONE, Direction.L), new DeltaResult(1, TapeSymbol.ONE, Direction.R) }, // move to blank and set it to one
			//                              V should be BLANK                   shoudl be ONE v
			{ new DeltaResult(3, TapeSymbol.BLANK, Direction.R), new DeltaResult(2, TapeSymbol.ONE, Direction.L) } // go back to first field of result
};
		// delta table for '-' function: arg1 - arg2 (with arg1 >= arg2)
		// input (args in unary, head on BLANK before arg1): BLANK arg1 BLANK arg2 BLANK
		// output: number starting with head
		public static final DeltaResult[][] deltaTableMinus = { 
				{ new DeltaResult(1, TapeSymbol.BLANK, Direction.R), new DeltaResult(0, TapeSymbol.ONE, Direction.R) }, // go to first blank, then go to checking state
		
				{ new DeltaResult(7, TapeSymbol.BLANK, Direction.L),  new DeltaResult(2, TapeSymbol.ONE, Direction.R)}, // This state checks for end of input, else go to state 2
				{ new DeltaResult(3, TapeSymbol.BLANK, Direction.L), new DeltaResult(2, TapeSymbol.ONE, Direction.R)}, // go to end of divisor; if at end goto state 3
				// BLANK case should not happen v
				{ new DeltaResult(10, TapeSymbol.BLANK, Direction.L), new DeltaResult(4, TapeSymbol.BLANK, Direction.L)}, // remove last field of divisor
				{ new DeltaResult(5, TapeSymbol.BLANK, Direction.L), new DeltaResult(4, TapeSymbol.ONE, Direction.L) }, // goto over blank to dividend
				{ new DeltaResult(6, TapeSymbol.BLANK, Direction.R), new DeltaResult(5, TapeSymbol.ONE, Direction.L)}, // go to beginning of dividend
				// BLANK case should not happen v
				{ new DeltaResult(10, TapeSymbol.BLANK, Direction.N), new DeltaResult(0, TapeSymbol.BLANK, Direction.R) }, // remove first field of dividend and start over
				{ new DeltaResult(7, TapeSymbol.BLANK, Direction.L), new DeltaResult(8, TapeSymbol.ONE, Direction.L)},
				{ new DeltaResult(10, TapeSymbol.BLANK, Direction.R), new DeltaResult(8, TapeSymbol.ONE, Direction.L)}
		};
				
}
