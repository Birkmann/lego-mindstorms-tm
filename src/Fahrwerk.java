import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.Port;
import lejos.robotics.RegulatedMotor;
import lejos.utility.Delay;

public class Fahrwerk {
	
	/*
	 * Das Fahrwerk der Turingmaschine
	 */
	
	private RegulatedMotor motor;
	public RegulatedMotor getMotor() { return this.motor; }
	private LeseKopf leseKopf;


	public Fahrwerk(Port port, int speed, LeseKopf leseKopf) {
		this.motor = new EV3LargeRegulatedMotor(port);
		this.motor.setSpeed(speed);
		this.leseKopf = leseKopf;
	}
	
	// move to the next "field" (brick) in the given direction
	public void move(Direction direction) {

		// Do not move in case of N
		if(direction == null) throw new Error("fatal: direction is null; should not happen");
		if(direction == Direction.N) return;
		// move backwards when R (motor is built in "backwards")
		else if(direction == Direction.R) this.motor.backward();
		// move forwards when L
		else this.motor.forward();
		
		// delay until sensing again; is ok if bricks are ~10cm off
		Delay.msDelay(TuringMachine.currentDrivingDelay);
		
		// Wait until leseKopf does not sense brick anymore so we can be sure we did not measure the last one twice
		while(leseKopf.senseBrick()) {
			Helper.delay();
		}
		
		// Wait until sensing brick again
		while(!leseKopf.senseBrick()) {
			Helper.delay();
		}
		
		// drive little bit further until reaching lowest point: The USLeseKopf reads higher ranges at when only near and not precisely before a brick
		this.leseKopf.waitWhileRangeIsDecreasing();
		
		// then stop
		this.motor.stop();
	}
}


