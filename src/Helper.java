import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;
import lejos.utility.Delay;

public class Helper {
	
	/*
	 * HELPER FUCTIONS (self explanatory)
	 */

	public static void delay() {
		Delay.msDelay(5);
	}
	
	public static void delayLong() {
		Delay.msDelay(10);
	}
	
	public static void writeClear(Object... args) {
		LCD.clearDisplay();
		for(Object o : args) System.out.println(o);
	}
	
	public static void waitButton() {
		writeClear("Press button");
		Button.waitForAnyPress();
	}
}
