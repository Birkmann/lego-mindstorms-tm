/*
 * The tape alphabet for the turing machine; currently consists of only one symbol (ONE) and the BLANK
 */
public enum TapeSymbol {
	BLANK, ONE
}

