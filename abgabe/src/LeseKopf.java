import lejos.hardware.lcd.LCD;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.RangeFinder;
import lejos.robotics.RangeFinderAdapter;
import lejos.robotics.RegulatedMotor;

public class LeseKopf {
	/*
		* Every brick in epsilon range or smaller than distance0 counts as BLANK
		* Every brick in epsilon range or smaller than distance1 counts as BLANK
		* Every other brick is an undefined symbol (null)
		*/

	// INVARIANT: distance0 < distance 1
	private RangeFinder sonar;
	

		
	public LeseKopf(Port port) {
		EV3UltrasonicSensor ussensor = new EV3UltrasonicSensor(port);
		this.sonar = new RangeFinderAdapter(ussensor);
	}

	// returns true iff a brick is in front of the ultrasensor
	public boolean senseBrick() {
		float range = this.sonar.getRange();
		return range < Math.max(TuringMachine.distance0, TuringMachine.distance1) + 3 * TuringMachine.epsilon;
	}
	
	// converts a distance to the appropriate TapeSymbol
	public TapeSymbol distanceToTapeSymbol(float range) {
		if(range >= TuringMachine.distance1 + 1.5 * TuringMachine.epsilon) return null;
		return range < TuringMachine.distance0 + 1*5 * TuringMachine.epsilon  ? TapeSymbol.BLANK
				: range < TuringMachine.distance1 + 1.5 * TuringMachine.epsilon  ? TapeSymbol.ONE
				: null;
	}
	
	@Deprecated
	public TapeSymbol getTapeSymbol() {
		float range = this.sonar.getRange();
		Helper.writeClear("Range: " + range);
		Helper.delayLong();
		return distanceToTapeSymbol(range);
	}
	
	// measure the distance to a brick precisely by "wiggling" a little bit to the left and right and making additional measurements
	public float getPrecisionDistance(Direction comingFromDirection, RegulatedMotor motor) {
		
		// adjust speed for precision reading
		motor.setSpeed(TuringMachine.messSpeed);
		
		// adjust angle
		int angle;
		if(comingFromDirection == Direction.R) {
			angle = - TuringMachine.precisionReadAngle; // driving right is a negative rotation
//			 move little bit closer to brick; Sensor ungenauer beim Rechtsbewegung des Fahrzeugs, deswegen erkennt er die seine später und muss nicht zusätzlich hinfahren
			motor.rotate(angle);
		}
		else if (comingFromDirection == Direction.L) {
			angle = TuringMachine.precisionReadAngle;
			// move little bit closer to brick (more than in RIGHT-case since the foerderband is asymmetric)
			motor.rotate(angle * 2);

		} else if (comingFromDirection == Direction.N) { // Do not care if it did not move
			angle = TuringMachine.precisionReadAngle;
			// Do not move closer in this case
		} else throw new Error("Direction other than L, N ,R: should not happen");
		// read three times
		float range1 = this.sonar.getRange();
		motor.rotate(angle);
		float range2 = this.sonar.getRange();
		motor.rotate(- 2 * angle);
		float range3 = this.sonar.getRange();
		motor.rotate(angle);

		
		// choose minimum to return as sonar outputs a range greater then the actual most of the time if the measurement is wrong
		float min = Math.min(Math.min(range1,  range2), range3);
		motor.setSpeed(TuringMachine.drivingSpeed);

		return min;
		
	}
	
	
	
	public float getRange() {
		float range =  this.sonar.getRange();
//		Helper.writeClear("Current range: ", range, "");
		return range;
	}
	
	// true iff x is close (in epsilon-range) to d
	private boolean inEpsRadiusOf(float x, float d) {
		return x >= d - 1 * TuringMachine.epsilon || x <= d + 1 * TuringMachine.epsilon;
	}
	
	// true iff x is close to one of the distances
	private boolean distanceInEpsilonRadius(float x) {
		return inEpsRadiusOf(x, TuringMachine.distance0) || inEpsRadiusOf(x, TuringMachine.distance1);
	}
	
	
	public void waitWhileRangeIsDecreasing() {
		float curRange = getRange();
		Helper.delay();
		float nextRange = getRange();
		
		while(nextRange < curRange || !distanceInEpsilonRadius(nextRange)) {
			Helper.delay();
			curRange = nextRange;
			nextRange = getRange();
		}
	}
}
