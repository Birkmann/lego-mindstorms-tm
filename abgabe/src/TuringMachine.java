import lejos.hardware.Button;
import lejos.hardware.Sound;
import lejos.hardware.lcd.LCD;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.robotics.RegulatedMotor;
import lejos.utility.Delay;

public class TuringMachine {

	/* 
	 * *** COMPONENTS ***
	 */
	private Fahrwerk fahrwerk;
	private LeseKopf leseKopf;
	private SchreibKopf schreibKopf;
	
	/* 
	 * *** CONSTANTS ***
	 */

	// Invariants:
	// * bricks should lie at least 10cm apart
	// * distance0 < distance 1
	// * epsilon < (distance1 - distance0) / 2
	// * Robot needs at least drivingDelay ms to travel between bricks
	// * robot arm should be set about (writingPositionAngle + rotationAngle) when in up position
	// * rotate(precisionReadAngle) ~ distance between the two eyes of the USSensor
	public static final int distance0 = 7; // The distance at which a brick is measured as a BLANK symbol
	public static final int distance1 = 14; // The distance at which a brick is measured as a ONE symbol
	public static final int epsilon = 1;  // erlaubte ungenauigkeit
	public static final int drivingSpeed = 130; // 
	public static final int messSpeed = 90; // the speed at which to wiggle while precision measuring
	public static final int drivingDelay = 500; // time after starting to drive that needs to pass before reading another symbol
	public static int currentDrivingDelay = drivingDelay; // time after starting to drive that needs to pass before reading another symbol; is a variable so it can be set to 0 after phantom read

	public static final int runterlasserSpeed = 100; // speed to let down SchreibKopf
	
	public static final int rotationAngle = 145; // angle to let down foerderband
	public static final int foerderbandSpeed = 300; // writing speed
	public static final int precisionReadAngle = 30; // the angle to rotate for precision measuring
	public static final int vol = 20; // volume
	public static final int dur = 300; // duration of the beep


	/*
	 * Variables of the internal automaton of the TM
	 */
	private int[] finalStates = { 3 };
	private int currentState = 0;
	private boolean done = false; // indicates whether the TM is in a final state
	// `delta` represents the deltaTable (Ueberfuehrungsfunktion):
	// first dimension indexes the state and second the tape symbol (BLANK or ONE)
	// returns the follow-configuration
	// delta :: States x TapeSymbol -> States x TapeSymbol x Direction
	private final DeltaResult[][] deltaTable; // transition table
	private Direction currentDirection = Direction.N;


	private DeltaResult readDeltaTable(TapeSymbol symbol) {
		DeltaResult[] stateArr = deltaTable[currentState];
		return  symbol == TapeSymbol.BLANK ? stateArr[0] : stateArr[1];
	}

	// returns true if the current state is final
	private boolean currentlyInFinalState() {
		for (int i = 0; i < finalStates.length; i++) { // Stop if currently in final step
			if (finalStates[i] == currentState) {
				return true;
			}
		}
		return false;
	}

	// A function helping with auditory keys
	private void beepForSymbol(TapeSymbol s) {
		if(s == TapeSymbol.ONE) Sound.playTone(440,dur, vol); // play high tone when reading a ONE
		else Sound.playTone(110,dur, vol); // play low tone when reading a BLANK
	}

	private void makeStep() {
		float d; // current distance measured
		TapeSymbol currentSymbol = null; // current tape symbol read; dependent of d
		// This loop ensures the TM stands precisely before a brick if the last move operation stopped to early
		do {
			// try to read symbol and keep on moving while none can be read
			d = this.leseKopf.getPrecisionDistance(currentDirection, this.fahrwerk.getMotor());
			currentSymbol = 		 this.leseKopf.distanceToTapeSymbol(d);
			if(currentSymbol == null) {
				TuringMachine.currentDrivingDelay = 0; // No delay after false measurement
				this.fahrwerk.move(currentDirection);
			}
		} while(currentSymbol == null);

		// from here the machine has correctly read a symbol; oringal drivingDelay can be restored for next iteration
		TuringMachine.currentDrivingDelay = TuringMachine.drivingDelay;

		// only here we can be sure the TM moved to the next field
		// otherwise we could take this condition into the outer loop, but then it would terminate too early (before reaching the next field)
		if(currentlyInFinalState()) {
			done = true;
			return;
		}

		// make sound corresponding to symbol
		beepForSymbol(currentSymbol);

		// to one computational step
		DeltaResult result = readDeltaTable(currentSymbol);
		Helper.delayLong();
		
		this.schreibKopf.writeSymbol(result.symbol, d);
		Helper.delayLong();

		this.currentState = result.state;
		Helper.delayLong();

		Helper.delayLong();
		this.currentDirection = result.direction;
		this.fahrwerk.move(result.direction);
	}

	
	public void run() {
		// init sequence
		Sound.setVolume(vol);
		Sound.beepSequenceUp();
		
		// Execute computation steps while not in a final state
		while (!done)
			makeStep();
		
		// end sequence
		LCD.clearDisplay();
		System.out.println("FINISHED");
		Sound.beepSequence();		
		Helper.waitButton();
	}

	// The default turingmachine, ports could be parameters but are rarely ever changed
	public TuringMachine(int[] finalStates, DeltaResult[][] table) {
		this.leseKopf = new LeseKopf(SensorPort.S2, distance0, distance1, epsilon);
		this.fahrwerk = new Fahrwerk(MotorPort.D, drivingSpeed, leseKopf);
		this.schreibKopf = new SchreibKopf(MotorPort.C, runterlasserSpeed, MotorPort.A, foerderbandSpeed, distance0, distance1, epsilon, leseKopf);
		this.finalStates = finalStates;
		this.deltaTable = table;
	}

	
	// test the TM with the plus function
	public static void testTMPlus() {
		 TuringMachine tm = new TuringMachine(new int[]{ 3 }, DeltaResult.deltaTablePlus);
		 tm.run();
	}
	
	// test the TM with the minus function
	public static void testTMMinus() {
		 TuringMachine tm = new TuringMachine(new int[]{ 10 }, DeltaResult.deltaTableMinus);
		 tm.run();
	}
	
	public static void main(String[] args) {
		testTMMinus();
//		testTMPlus();
	}

}
