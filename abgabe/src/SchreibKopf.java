import lejos.hardware.Button;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.Port;
import lejos.robotics.RegulatedMotor;
import lejos.utility.Delay;

public class SchreibKopf {

	/*
	 * Der Schreibkopf der Turingmaschine
	 */
	
	private RegulatedMotor runterlasser;
	private int runterlasserSpeed;
	private RegulatedMotor foerderband;
	private LeseKopf leseKopf;
	// the distance a brick is transportated by one rotation of the foerderband
	private static final double foerderband_oneRotation_distanceCovered = 9.5;

	// the radius to rotate the foerderband to convert a blank to a one
	public int radiusBlankToOne(float d) {
		// add epsilon/2 since the foerderband moves the bricks a little too short
		float toTravelCm = TuringMachine.distance1 - d + TuringMachine.epsilon / 2;
		double radiusToTurn = (360 * toTravelCm / 9.5);
		return - (int) radiusToTurn;
	}

	// the radius to rotate the foerderband to convert a blank to a one
	public int radiusOneToBlank(float d) {

		float toTravelCm = d - TuringMachine.distance0 + TuringMachine.epsilon / 2;
		double radiusToTurn = (360 * toTravelCm / 9.5);
		return (int) Math.round(radiusToTurn);
	}

	public SchreibKopf(Port runterlasserPort, int runterlasserSpeed, Port foerderbandPort, int foerderbandSpeed, LeseKopf leseKopf) {
		this.runterlasser = new EV3LargeRegulatedMotor(runterlasserPort);
		runterlasser.setSpeed(runterlasserSpeed);
		this.runterlasserSpeed = runterlasserSpeed;

		this.foerderband = new EV3LargeRegulatedMotor(foerderbandPort);
		foerderband.setSpeed(foerderbandSpeed);

		this.leseKopf = leseKopf;
	}
	

	// schreibkopf runterfahren
	private void runterfahren() {
		this.runterlasser.rotate(TuringMachine.rotationAngle / 10 * 8);
		runterlasser.setSpeed(runterlasserSpeed / 4);
		this.runterlasser.rotate(TuringMachine.rotationAngle / 10 * 2);
	}
	
	// schreibkopf hochfahren
	private void hochfahren() {
		this.runterlasser.rotate(- TuringMachine.rotationAngle / 10 * 2);
		runterlasser.setSpeed(runterlasserSpeed);
		this.runterlasser.rotate(- TuringMachine.rotationAngle / 10 * 8);
	}

	// write symbol 'symbol' on the tape where the distance of the current brick is 'd'
	public void writeSymbol(TapeSymbol symbol, float currentBrickDistance) {
		
		// if the symbol to write is already on tape do nothing;
		if (this.leseKopf.distanceToTapeSymbol(currentBrickDistance) == symbol) {
			Helper.writeClear("Symbol " + symbol.toString(), " already on Tape");
			Delay.msDelay(1000);
			return;
		}

		if (symbol == null)
			throw new Error("fatal: should not happen; this null should be handled in TuringMachine.step()");

		// get the radius to rotate the foerderband
		int travelRadius = (symbol == TapeSymbol.BLANK) ? radiusOneToBlank(currentBrickDistance) : radiusBlankToOne(currentBrickDistance);
		
		// Kopf runterlassen
		runterfahren();
		Delay.msDelay(100);
		// rotieren
		this.foerderband.rotate(travelRadius);
		Delay.msDelay(100);
		// Kopf hochfahren
		hochfahren();
		
		

	}
}
